#include <Ultrasonic.h>
#include <LiquidCrystal.h>
#define trig 2
#define echo 1
LiquidCrystal lcd(12, 11, 6, 5, 4, 3);

Ultrasonic ultrasonic(trig, echo);
 
void setup()
{
 lcd.begin(16,2); 
}
 
void loop()
{
  float cmMsec, inMsec;
  long microsec = ultrasonic.timing();
  cmMsec = ultrasonic.convert(microsec, Ultrasonic::CM);
  cmMsec=floor(cmMsec);
  Serial.print(cmMsec);
   Serial.println("cm"); 

 lcd.clear();
  lcd.setCursor(3, 0);
  lcd.print(cmMsec);
   lcd.setCursor(7, 0);
  lcd.print("cm");

  lcd.setCursor(6, 1);
  lcd.print("KDT");

  
  delay(1000);
}
